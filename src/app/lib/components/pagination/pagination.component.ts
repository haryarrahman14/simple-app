import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  standalone: true,
  selector: 'app-pagination',
  imports: [CommonModule],
  templateUrl: './pagination.component.html',
})
export class PaginationComponent {
  @Input() pageSize: number = 10;
  @Input() totalData: number = 0;
  @Input() pageNumber: number = 1;
  @Input() totalDataPerPage: number = 0;
  @Input() goToPreviousPage: () => void = () => {};
  @Input() goToNextPage: () => void = () => {};

  constructor() {}

  get maxPage(): number {
    return Math.ceil(this.totalData / this.pageSize);
  }

  get pageRange(): number[] {
    const maxPageToShow = Math.min(this.maxPage, 5);
    const startPage = Math.max(
      1,
      this.pageNumber - Math.floor(maxPageToShow / 2)
    );
    return Array.from({ length: maxPageToShow }, (_, i) => startPage + i);
  }
}
