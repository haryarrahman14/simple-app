import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  standalone: true,
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  imports: [CommonModule],
})
export class AlertComponent {
  @Input() showAlert: boolean = false;
  @Input() onClose: () => void = () => {};
  @Input() alertType: 'success' | 'warning' | 'info' | 'error' = 'error';
  @Input() alertMessage: string = 'This is an alert message';

  get alertClasses() {
    const baseClasses = 'text-white px-6 py-4 border-0 rounded relative mb-4';
    switch (this.alertType) {
      case 'success':
        return `${baseClasses} bg-green-500`;
      case 'warning':
        return `${baseClasses} bg-yellow-500`;
      case 'info':
        return `${baseClasses} bg-blue-500`;
      case 'error':
      default:
        return `${baseClasses} bg-red-500`;
    }
  }
}
