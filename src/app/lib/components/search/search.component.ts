import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  standalone: true,
  selector: 'app-search',
  templateUrl: 'search.component.html',
  imports: [FormsModule, ReactiveFormsModule],
})
export class SearchComponent {
  @Input() placeholder: string = 'Search...';
  @Input() searchQuery: string = '';
  @Output() search: EventEmitter<string> = new EventEmitter<string>();

  onSearch(event: Event): void {
    event.preventDefault();
    this.search.emit(this.searchQuery);
  }
}
