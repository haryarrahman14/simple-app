import { CommonModule } from '@angular/common';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TableColumn } from '@lib/interfaces';

@Component({
  selector: 'app-table',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './table.component.html',
})
export class TableComponent {
  @Input() columns: TableColumn<any>[] = [];
  @Input() data: any[] = [];
  @Input() sortAction: (key: any) => void = () => {};
  @Output() rowEdit = new EventEmitter<any>();
  @Output() rowDelete = new EventEmitter<any>();
  @Output() rowClick = new EventEmitter<any>();

  onClickRow(event: Event, item: any) {
    event.preventDefault();
    this.rowClick.emit(item);
  }

  onClickEdit(event: Event, item: any) {
    event.preventDefault();
    this.rowEdit.emit(item);
  }

  onClickDelete(event: Event, item: any) {
    event.preventDefault();
    this.rowDelete.emit(item);
  }
}
