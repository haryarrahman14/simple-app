import { CommonModule } from '@angular/common';
import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { createPopper } from '@popperjs/core';

@Component({
  standalone: true,
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  imports: [CommonModule],
})
export class DropdownComponent implements AfterViewInit {
  @Input() label: string = '';
  @Input() dropdownItems: { label: string; value: any }[] = [];
  @Output() itemSelected = new EventEmitter<any>();

  @ViewChild('btnDropdownRef', { static: false }) btnDropdownRef?: ElementRef;
  @ViewChild('popoverDropdownRef', { static: false })
  popoverDropdownRef?: ElementRef;
  dropdownPopoverShow = false;

  ngAfterViewInit() {
    createPopper(
      this.btnDropdownRef?.nativeElement,
      this.popoverDropdownRef?.nativeElement,
      {
        placement: 'bottom-start',
      }
    );
  }

  toggleDropdown(event: Event) {
    event.preventDefault();
    this.dropdownPopoverShow = !this.dropdownPopoverShow;
  }

  selectItem(value: any) {
    this.itemSelected.emit(value);
    this.dropdownPopoverShow = false;
  }
}
