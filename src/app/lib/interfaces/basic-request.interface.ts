export interface BasicRequest {
  page: number;
  size: number;
  search: string;
}
