import { BasicRequest } from './basic-request.interface';

export interface Employee {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: number;
  status: string;
  group: string;
  description: string;
}

export interface EmployeeRequest extends BasicRequest {
  sortField: keyof Employee;
  sortOrder: 'asc' | 'desc';
}
