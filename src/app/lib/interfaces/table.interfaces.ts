export interface TableColumn<T> {
  header: string;
  key: keyof T;
  sortable?: boolean;
}
