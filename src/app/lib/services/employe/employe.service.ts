import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Employee } from '@lib/interfaces';
import employeeJSON from '@assets/dummy-data/employees.json';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private employees: Employee[] = employeeJSON;

  getTotalEmployeeCount(): Observable<number> {
    return of(this.employees.length);
  }

  getEmployees(
    page: number = 1,
    size: number = 10,
    sortField: keyof Employee = 'username',
    sortOrder: 'asc' | 'desc' = 'asc',
    search: string = ''
  ): Observable<Employee[]> {
    let filteredEmployees = [...this.employees];

    if (search) {
      filteredEmployees = filteredEmployees.filter((employee) =>
        Object.values(employee).some((value) =>
          value.toString().toLowerCase().includes(search.toLowerCase())
        )
      );
    }

    filteredEmployees.sort((a, b) => {
      const fieldA = a[sortField];
      const fieldB = b[sortField];
      let comparison = 0;
      if (fieldA > fieldB) {
        comparison = 1;
      } else if (fieldA < fieldB) {
        comparison = -1;
      }
      return sortOrder === 'asc' ? comparison : -comparison;
    });

    const start = (page - 1) * size;
    const end = start + size;
    return of(filteredEmployees.slice(start, end));
  }

  getEmployeeById(username: string): Observable<Employee | undefined> {
    const employee = this.employees.find((emp) => emp.username === username);
    return of(employee);
  }

  addEmployee(employee: Employee): Observable<void> {
    this.employees.push(employee);
    return of(undefined);
  }

  editEmployee(username: string, updatedEmployee: Employee): Observable<void> {
    const index = this.employees.findIndex((emp) => emp.username === username);
    if (index !== -1) {
      this.employees[index] = updatedEmployee;
    }
    return of(undefined);
  }

  deleteEmployee(username: string): Observable<void> {
    this.employees = this.employees.filter((emp) => emp.username !== username);
    return of(undefined);
  }
}
