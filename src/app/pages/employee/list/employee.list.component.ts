import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { Router } from '@angular/router';
import {
  ButtonComponent,
  TableComponent,
  PaginationComponent,
  SearchComponent,
  DropdownComponent,
  AlertComponent,
  ConfirmDeleteDialogComponent,
} from '@lib/components';
import { Employee, EmployeeRequest, TableColumn } from '@lib/interfaces';
import { AuthService } from '@lib/services';
import { EmployeeService } from '@lib/services/employe/employe.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  standalone: true,
  imports: [
    CommonModule,
    TableComponent,
    ButtonComponent,
    PaginationComponent,
    SearchComponent,
    DropdownComponent,
    AlertComponent,
  ],
  templateUrl: './employee.list.component.html',
})
export class EmployeeListComponent implements OnInit {
  private readonly _router = inject(Router);
  private readonly _authService = inject(AuthService);

  columns: TableColumn<Employee>[] = [
    { key: 'username', header: 'Username', sortable: true },
    { key: 'firstName', header: 'First Name', sortable: true },
    { key: 'lastName', header: 'Last Name', sortable: true },
    { key: 'email', header: 'Email', sortable: true },
    { key: 'birthDate', header: 'Birth Date', sortable: true },
    { key: 'basicSalary', header: 'Salary', sortable: true },
    { key: 'status', header: 'Status', sortable: true },
    { key: 'group', header: 'Group', sortable: true },
    { key: 'description', header: 'Description', sortable: false },
  ];

  labelSize: string = 'Size';
  dropdownItems = [
    { label: '5', value: 5 },
    { label: '10', value: 10 },
    { label: '25', value: 25 },
    { label: '100', value: 100 },
  ];

  employees: Employee[] = [];
  totalEmployees: number = 0;
  page: EmployeeRequest['page'] = 1;
  size: EmployeeRequest['size'] = 10;
  sortField: EmployeeRequest['sortField'] = 'username';
  sortOrder: EmployeeRequest['sortOrder'] = 'asc';
  search: EmployeeRequest['search'] = '';

  showAlert: boolean = false;
  alertType: 'success' | 'warning' | 'info' | 'error' = 'success';
  alertMessage: string = '';

  constructor(
    private employeeService: EmployeeService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.loadEmployees();
    this.loadTotalEmployees();
  }

  loadTotalEmployees(): void {
    this.employeeService.getTotalEmployeeCount().subscribe((data) => {
      this.totalEmployees = data;
    });
  }

  loadEmployees = (): void => {
    this.employeeService
      .getEmployees(
        this.page,
        this.size,
        this.sortField,
        this.sortOrder,
        this.search
      )
      .subscribe((data) => {
        this.employees = data;
      });
  };

  handleItemSelectedDropdown(value: number) {
    this.labelSize = value.toString();
    this.size = value;
    this.loadEmployees();
  }

  onSearch(search: string): void {
    this.search = search;
    this.loadEmployees();
  }

  onSort(field: EmployeeRequest['sortField']) {
    this.sortField = field;
    this.sortOrder = this.sortOrder === 'asc' ? 'desc' : 'asc';
    this.loadEmployees();
  }

  onPageChange(page: EmployeeRequest['page']) {
    this.page = page;
    this.loadEmployees();
  }

  goToPreviousPage() {
    if (this.page > 1) {
      this.onPageChange(this.page - 1);
    }
  }

  goToNextPage() {
    if (this.page < Math.ceil(this.totalEmployees / this.size)) {
      this.onPageChange(this.page + 1);
    }
  }

  onSizeChange(size: EmployeeRequest['size']) {
    this.size = size;
    this.loadEmployees();
  }

  sortAction(field: keyof Employee) {
    this.sortField = field;
    this.sortOrder = this.sortOrder === 'asc' ? 'desc' : 'asc';
    this.loadEmployees();
  }

  onClickRow(employee: Employee) {
    this._router.navigate(['/employee/detail', employee.username]);
  }

  onClickEdit(employee: Employee) {
    console.log('masuk sini');
    this._router.navigate(['/employee/edit', employee.username]);
  }

  onClickDelete(employee: Employee) {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      width: '300px',
      data: { label: `${employee.username}` },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.employeeService.deleteEmployee(employee.username).subscribe(() => {
          this.alertType = 'success';
          this.alertMessage = 'Delete data successfully!';
          this.showAlert = true;
          this.loadEmployees();
        });
      }
    });
  }

  onCloseAlert() {
    this.showAlert = false;
  }

  addEmployee(): void {
    this._router.navigate(['/employee/create']);
  }

  logout(): void {
    this._authService.logout();
    this._router.navigate(['/auth/login']);
  }
}
