import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';

import dummyDataGroups from '@assets/dummy-data/groups.json';
import { Router } from '@angular/router';
import { EmployeeService } from '@lib/services';
import { Employee } from '@lib/interfaces';

@Component({
  standalone: true,
  selector: 'app-employee-form',
  templateUrl: './employee.create.component.html',
  imports: [
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    ReactiveFormsModule,
    CommonModule,
  ],
})
export class EmployeeCreateComponent {
  private _router = inject(Router);
  private _employeeService = inject(EmployeeService);

  employeeForm: FormGroup;
  submitted = false;
  groups = dummyDataGroups;

  constructor(private formBuilder: FormBuilder) {
    this.employeeForm = this.formBuilder.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: ['', [Validators.required, this.maxDateValidator]],
      basicSalary: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  maxDateValidator(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    if (control.value && new Date(control.value) > new Date()) {
      return { maxDate: true };
    }
    return null;
  }

  onSubmit() {
    this.submitted = true;

    if (this.employeeForm.invalid) {
      return;
    }

    const newEmployee: Employee = this.employeeForm.value;

    this._employeeService.addEmployee(newEmployee).subscribe(() => {
      this._router.navigate(['']);
    });
  }

  onCancel() {
    this._router.navigate(['']);
  }
}
