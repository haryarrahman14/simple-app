import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ButtonComponent } from '@lib/components';
import { EmployeeService } from '@lib/services';

interface Employee {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: number;
  status: string;
  group: string;
  description: string;
}

@Component({
  standalone: true,
  templateUrl: './employee.detail.component.html',
  imports: [CommonModule, ButtonComponent],
})
export class EmployeeDetailComponent implements OnInit {
  employee: Employee = {
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    birthDate: '',
    basicSalary: 0,
    status: '',
    group: '',
    description: '',
  };
  showAlert: boolean = false;
  alertType: string = '';
  alertMessage: string = '';

  private readonly _router = inject(Router);

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService
  ) {}

  ngOnInit() {
    const username = this.route.snapshot.paramMap.get('username');
    if (username) {
      this.employeeService.getEmployees().subscribe((employees) => {
        this.employee =
          employees.find((emp) => emp.username === username) ?? this.employee;
        if (!this.employee) {
          this.showAlert = true;
          this.alertType = 'error';
          this.alertMessage = 'Employee not found';
        }
      });
    }
  }

  formatCurrency(value: number): string {
    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    }).format(value);
  }

  goBackToList() {
    this._router.navigate(['/'], {
      queryParamsHandling: 'preserve',
    });
  }

  onCloseAlert() {
    this.showAlert = false;
  }
}
