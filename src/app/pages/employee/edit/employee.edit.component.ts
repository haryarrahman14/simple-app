import { CommonModule } from '@angular/common';
import { Component, inject, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';

import dummyDataGroups from '@assets/dummy-data/groups.json';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '@lib/services';
import { Employee } from '@lib/interfaces';

@Component({
  standalone: true,
  selector: 'app-employee-form',
  templateUrl: './employee.edit.component.html',
  imports: [
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    ReactiveFormsModule,
    CommonModule,
  ],
})
export class EmployeeEditComponent implements OnInit {
  private _router = inject(Router);
  private _route = inject(ActivatedRoute);
  private _employeeService = inject(EmployeeService);

  employeeForm: FormGroup;
  submitted = false;
  groups = dummyDataGroups;
  employeeId: string | null = null;

  constructor(private formBuilder: FormBuilder) {
    this.employeeForm = this.formBuilder.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: ['', [Validators.required, this.maxDateValidator]],
      basicSalary: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.employeeId = this._route.snapshot.paramMap.get('username');
    if (this.employeeId) {
      this._employeeService
        .getEmployeeById(this.employeeId)
        .subscribe((employee: Employee | undefined) => {
          if (employee) {
            this.employeeForm.patchValue(employee);
          } else {
            console.error(
              `Employee with username ${this.employeeId} not found.`
            );
          }
        });
    }
  }

  maxDateValidator(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    if (control.value && new Date(control.value) > new Date()) {
      return { maxDate: true };
    }
    return null;
  }

  onSubmit() {
    this.submitted = true;

    if (this.employeeForm.invalid) {
      return;
    }

    const newEmployee: Employee = this.employeeForm.value;

    if (this.employeeId) {
      this._employeeService
        .editEmployee(this.employeeId, newEmployee)
        .subscribe(() => {
          this._router.navigate(['']);
        });
    } else {
      this._employeeService.addEmployee(newEmployee).subscribe(() => {
        this._router.navigate(['']);
      });
    }
  }

  onCancel() {
    this._router.navigate(['']);
  }
}
