import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    title: 'Employee',
    loadComponent: async () =>
      (await import('./list/employee.list.component')).EmployeeListComponent,
  },
  {
    path: 'employee/create',
    title: 'Create Employee',
    loadComponent: async () =>
      (await import('./create/employee.create.component'))
        .EmployeeCreateComponent,
  },
  {
    path: 'employee/detail/:username',
    loadComponent: async () =>
      (await import('./detail/employee.detail.component'))
        .EmployeeDetailComponent,
  },
  {
    path: 'employee/edit/:username',
    loadComponent: async () =>
      (await import('./edit/employee.edit.component')).EmployeeEditComponent,
  },
];
