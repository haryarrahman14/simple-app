import { CommonModule } from '@angular/common';
import {
  ReactiveFormsModule,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Component, Input, inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@lib/services';

@Component({
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: 'login.component.html',
})
export class LoginComponent {
  @Input() returnUrl!: string;

  private readonly _router = inject(Router);
  private readonly _authService = inject(AuthService);

  loginForm: FormGroup;
  submitted: boolean = false;
  showPassword: boolean = false;

  constructor(private formBuilder: FormBuilder) {
    this.loginForm = this.formBuilder.group({
      email: ['admin@simpleapp.com', [Validators.required, Validators.email]],
      password: ['admin123', Validators.required],
    });
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this._authService.login();

    this._router.navigate([this.returnUrl ?? `/`]);
  }

  onShowPassword(): void {
    this.showPassword = !this.showPassword;
  }

  redirectSignUp(): void {
    /** SOON */
    // this._router.navigate(['/auth/register']);
  }

  redirectForgotPassword(): void {
    /** SOON */
    // this._router.navigate(['/auth/forgot-password']);
  }
}
